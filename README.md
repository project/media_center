# Media Center

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Drupal 8 module that installs a Media Center.

REQUIREMENTS
------------

  * TBD

INSTALLATION
------------

  * Install the module as you normally would.
  * Verify installation by visiting TBD.

CONFIGURATION
-------------

  * TBD

MAINTAINERS
-----------

Current maintainers:
  * [thejimbirch](https://www.drupal.org/u/thejimbirch)

This project has been sponsored by:
  * [Kanopi Studios](https://www.kanopi.com)
