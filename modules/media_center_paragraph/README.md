# Media Center Paragraph

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Installs a Paragraph bundle that references Media.


REQUIREMENTS
------------

  * Drupal Core Media
  * [Inline Entity Form](https://www.drupal.org/project/inline_entity_form)
  * [Paragraphs](https://www.drupal.org/project/paragraphs)


INSTALLATION
------------

  * Install the module as you normally would.
  * Verify installation by visiting /admin/structure/paragraphs_type and
  verifying your new Paragraph bundle is in the list.


CONFIGURATION
-------------

  * Add or edit an Entity revisions, Paragraphs field.
  * Check the box next to the Media bundle to add this bundle and save.


MAINTAINERS
-----------

Current maintainers:
  * [thejimbirch](https://www.drupal.org/u/thejimbirch)

This project has been sponsored by:
  * [Kanopi Studios](https://www.kanopi.com)
